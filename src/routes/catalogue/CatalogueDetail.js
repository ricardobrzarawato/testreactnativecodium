import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  PixelRatio,
  SafeAreaView,
  StatusBar,
  Image
} from "react-native";
import PropTypes from "prop-types";
import { ScrollView } from "react-native-gesture-handler";
import { Toolbar } from "react-native-material-ui";
import { Actions } from "react-native-router-flux";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    backgroundColor: "#019afd"
  },
  containerGeneral: {
    backgroundColor: "#fff",
    flex: 1
  },
  img: {
    height: 280,
    width: 280
  },
  textName: {
    fontSize: Platform.OS === "android" ? 18 / PixelRatio.getFontScale() : 16,
    color: "#4d4d4d",
    fontWeight: "bold"
  },
  containerName: {
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  textInfo: {
    fontSize: Platform.OS === "android" ? 16 / PixelRatio.getFontScale() : 14,
    color: "#4d4d4d",
    marginTop: 15
  },
  text: {
    fontSize: Platform.OS === "android" ? 16 / PixelRatio.getFontScale() : 14,
    color: "#4d4d4d",
    fontWeight: "bold"
  },
  textDamage: {
    marginTop: 10,
    fontSize: Platform.OS === "android" ? 18 / PixelRatio.getFontScale() : 16
  },
  center: {
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 25
  },
  padding: {
    padding: 20
  }
});
const CatalogueDetail = ({ data }) => (
  <SafeAreaView style={styles.container}>
    <Toolbar
      leftElement="arrow-back"
      centerElement="Detalle"
      onLeftElementPress={Actions.pop}
    />
    <View style={styles.containerGeneral}>
      <ScrollView contentContainerStyle={styles.center}>
        <View style={styles.padding}>
          <View style={styles.containerName}>
            <Text style={styles.textName}>{data && data.name}</Text>
          </View>
          <Image
            source={{ uri: data.imageUrl }}
            style={styles.img}
            resizeMode="contain"
          />
        </View>
        <View>
          {data && data.hp ? (
            <Text style={styles.textInfo}>
              Vida: <Text style={styles.text}>{data && data.hp}</Text>
            </Text>
          ) : null}
          {data && data.artist ? (
            <Text style={styles.textInfo}>
              Artista: <Text style={styles.text}>{data && data.artist}</Text>
            </Text>
          ) : null}
          {data && data.rarity ? (
            <Text style={styles.textInfo}>
              Rareza: <Text style={styles.text}>{data && data.rarity}</Text>
            </Text>
          ) : null}
          <Text style={styles.textInfo}>
            Serie: <Text style={styles.text}>{data && data.series}</Text>
          </Text>
          <Text style={styles.textInfo}>
            Conjunto: <Text style={styles.text}>{data && data.set}</Text>
          </Text>
          {data && data.attacks ? (
            <View>
              <Text style={[styles.text, styles.textDamage]}>Ataques</Text>
              {data &&
                data.attacks.map((Element, key) => (
                  <View key={key}>
                    <Text style={styles.textInfo}>
                      Nombre: <Text style={styles.text}>{Element.name}</Text>
                      {Element.damage !== "" ? (
                        <Text style={styles.textInfo}>
                          {", "}
                          Daño:{" "}
                          <Text style={styles.text}>{Element.damage}</Text>
                        </Text>
                      ) : null}
                    </Text>
                  </View>
                ))}
            </View>
          ) : null}
          {data && data.resistances ? (
            <View>
              <Text style={[styles.text, styles.textDamage]}>Resistencia</Text>
              {data &&
                data.resistances.map((Element, key) => (
                  <View key={key}>
                    <Text style={styles.textInfo}>
                      Tipo: <Text style={styles.text}>{Element.type}</Text>
                      {Element.value !== "" ? (
                        <Text style={styles.textInfo}>
                          {", "}
                          Valor:{" "}
                          <Text style={styles.text}>{Element.value}</Text>
                        </Text>
                      ) : null}
                    </Text>
                  </View>
                ))}
            </View>
          ) : null}
          {data && data.weaknesses ? (
            <View>
              <Text style={[styles.text, styles.textDamage]}>Debilidades</Text>
              {data &&
                data.weaknesses.map((Element, key) => (
                  <View key={key}>
                    <Text style={styles.textInfo}>
                      Tipo: <Text style={styles.text}>{Element.type}</Text>
                      {Element.value !== "" ? (
                        <Text style={styles.textInfo}>
                          {", "}
                          Valor:{" "}
                          <Text style={styles.text}>{Element.value}</Text>
                        </Text>
                      ) : null}
                    </Text>
                  </View>
                ))}
            </View>
          ) : null}
        </View>
      </ScrollView>
    </View>
  </SafeAreaView>
);

CatalogueDetail.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    imageUrl: PropTypes.string,
    weaknesses: PropTypes.array
  })
};
export default CatalogueDetail;
