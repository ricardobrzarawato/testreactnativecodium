import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  Platform,
  PixelRatio,
  SafeAreaView,
  StatusBar
} from "react-native";
import { Toolbar } from "react-native-material-ui";
import { Actions } from "react-native-router-flux";
import { useDispatch, useSelector } from "react-redux";
import PokemonCard from "../../components/PokemonCard";
import catalogueStart from "../../ducks/catalogue/actions";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fafafa",
    flex: 1
  },
  containerList: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    flex: 1
  },
  containerLoad: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  center: {
    justifyContent: "center",
    alignItems: "center"
  },
  containerMessage: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 200
  },
  textMessage: {
    fontSize: Platform.OS === "android" ? 18 / PixelRatio.getFontScale() : 16,
    color: "red",
    fontWeight: "900"
  },
  containerError: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

const CatalogueList = () => {
  const dispatch = useDispatch();
  const catalogueStore = useSelector(state => state.catalogueStore);
  const [message, setMessage] = useState("");
  const [item, setItem] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    // Este dispatch se encarga de hacer la llamada para obtener la lista del catalogo.
    dispatch(
      catalogueStart("v1/cards?subtype=Basic", Response => {
        if (!Response.bool) {
          setError(Response.message);
        }
      })
    );
  }, [dispatch]);

  const handleSearchName = text => {
    // Esta función se encarga de hacer un filtro de busqueda de pokemones por nombre.
    const catalogueJSON = JSON.stringify(catalogueStore.data);
    const parseCatalogue = JSON.parse(catalogueJSON);
    if (text !== "") {
      const pokemon = parseCatalogue.filter(film =>
        film.name.toLowerCase().indexOf(text.toLowerCase()) > -1
          ? film.name
          : null
      );
      if (pokemon.length > 0) {
        setItem(pokemon);
        setMessage("");
      } else {
        setMessage("Pokemon no encontrado");
      }
    } else {
      setMessage("");
      setItem([]);
    }
  };

  const handleClickClose = () => {
    setMessage("");
    setItem([]);
  };

  const handleClickDetailPokemon = dataValue => () => {
    // Esta función se encarga de ver el detalle de un pokemon.
    Actions.catalogueDetail({ data: dataValue });
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        backgroundColor: "#019afd"
      }}
    >
      <View style={styles.container}>
        <Toolbar
          centerElement="Buscar pokemon"
          searchable={{
            autoFocus: true,
            placeholder: "Buscar..",
            onChangeText: text => handleSearchName(text),
            onSearchClosed: value => handleClickClose(value)
          }}
        />
        {error ? (
          <View style={styles.containerError}>
            <Text style={styles.textMessage}>{error}</Text>
          </View>
        ) : (
          <View style={styles.containerList}>
            {catalogueStore && catalogueStore.loading ? (
              <View style={styles.containerLoad}>
                <ActivityIndicator color="#019afd" size={25} />
              </View>
            ) : (
              <View>
                {catalogueStore && catalogueStore.data ? (
                  <View>
                    {message ? (
                      <View style={styles.containerMessage}>
                        <Text style={styles.textMessage}>{message}</Text>
                      </View>
                    ) : (
                      <FlatList
                        data={item.length > 0 ? item : catalogueStore.data}
                        renderItem={data => (
                          <PokemonCard
                            item={data}
                            detail={e => handleClickDetailPokemon(e)}
                          />
                        )}
                        contentContainerStyle={styles.center}
                      />
                    )}
                  </View>
                ) : null}
              </View>
            )}
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default CatalogueList;
