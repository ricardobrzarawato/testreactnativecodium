import React from "react";
import { View } from "react-native";
import { Router, Scene, Lightbox, Stack } from "react-native-router-flux";
import CatalogueList from "./catalogue/CatalogueList";
import CatalogueDetail from "./catalogue/CatalogueDetail";

const Routes = () => (
  <View style={{ flex: 1 }}>
    <Router>
      <Lightbox>
        <Stack key="root">
          <Scene
            key="catalogueList"
            component={CatalogueList}
            initial
            hideNavBar
          />
          <Scene key="catalogueDetail" component={CatalogueDetail} hideNavBar />
        </Stack>
      </Lightbox>
    </Router>
  </View>
);

export default Routes;
