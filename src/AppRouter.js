import React from "react";
import { View } from "react-native";
import { PersistGate } from "redux-persist/lib/integration/react";
import { Provider } from "react-redux";
import configureStore from "./ducks";
import rootSaga from "./ducks/sagas";
import Routes from "./routes";

const { store, persistor, sagaMiddleware } = configureStore();
sagaMiddleware.run(rootSaga);

const AppRouter = () => (
  <View style={{ flex: 1, width: "100%" }}>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Routes />
      </PersistGate>
    </Provider>
  </View>
);

export default AppRouter;
