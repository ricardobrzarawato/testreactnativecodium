import axios from "axios";

const BASE_URL = "https://api.pokemontcg.io/";

const apiCall = (url, data, headers, method) =>
  axios({
    method,
    url: BASE_URL + url,
    data,
    headers
  });

export default apiCall;
