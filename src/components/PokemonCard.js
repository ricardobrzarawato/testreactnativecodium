import React from "react";
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  View,
  PixelRatio,
  Platform
} from "react-native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  container: {
    height: 270,
    width: 230,
    backgroundColor: "#fff",
    marginBottom: 15,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  containerImg: {
    height: 220,
    width: 220
  },
  containerText: {
    marginTop: 10
  },
  textName: {
    fontSize: Platform.OS === "android" ? 18 / PixelRatio.getFontScale() : 16,
    color: "#4d4d4d",
    fontWeight: "900"
  }
});
const PokemonCard = ({ item, detail }) => (
  <TouchableOpacity style={styles.container} onPress={detail(item.item)}>
    <Image
      source={{ uri: item && item.item.imageUrl }}
      style={styles.containerImg}
      resizeMode="contain"
    />
    <View style={styles.containerText}>
      <Text style={styles.textName}>{item && item.item.name}</Text>
    </View>
  </TouchableOpacity>
);

PokemonCard.propTypes = {
  item: PropTypes.shape({
    item: PropTypes.shape({ image: PropTypes.string, name: PropTypes.string })
  }),
  detail: PropTypes.func
};

export default PokemonCard;
