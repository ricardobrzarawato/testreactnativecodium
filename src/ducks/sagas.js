import {all} from 'redux-saga/effects';
import catalogue from './catalogue/sagas';

export default function* rootSaga() {
  yield all([catalogue()]);
}
