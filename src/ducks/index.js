import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import createSagaMiddleware from "redux-saga";
import { AsyncStorage } from "react-native";

import catalogueStore from "./catalogue";

const reducers = combineReducers({
  catalogueStore
});

const persistConfig = {
  key: "testReactNative",
  storage: AsyncStorage
};

const rootReducer = (state, action) => reducers(state, action);

const persistedReducer = persistReducer(persistConfig, rootReducer);
const sagaMiddleware = createSagaMiddleware();
export default function configureStore() {
  const store = createStore(
    persistedReducer,
    compose(applyMiddleware(sagaMiddleware))
  );
  const persistor = persistStore(store);
  return { store, persistor, sagaMiddleware };
}
