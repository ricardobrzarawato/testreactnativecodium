import { put, call, takeLatest } from "redux-saga/effects";
import * as types from "./types";
import apiCall from "../../api";

function* catalogueStart({ payload }) {
  // Esta funcion es para obtener los datos de la lista de los pokemones.
  try {
    const result = yield call(apiCall, payload.url, null, null, "GET");
    if (result.data && result.data.cards) {
      if (Array.isArray(result.data.cards) && result.data.cards.length > 0) {
        yield put({
          type: types.CATALOGUE_COMPLETE,
          result: result.data.cards
        });
      }
    }
  } catch (error) {
    if (error.response !== undefined) {
      if (error.response.status === 404) {
        const errorMessage = {
          message: "Recurso no encontrado.",
          bool: false
        };
        payload.callback(errorMessage);
        yield put({ type: types.CATALOGUE_ERROR });
      } else if (error.response.status === 500) {
        const errorMessage = {
          message: "Disculpe ha ocurrido un error interno.",
          bool: false
        };
        payload.callback(errorMessage);
        yield put({ type: types.CATALOGUE_ERROR });
      }
    } else {
      const errorMessage = {
        message: "Por favor revise su conexión a internet.",
        bool: false
      };
      payload.callback(errorMessage);
      yield put({ type: types.CATALOGUE_ERROR });
    }
  }
}

export default function* catalogue() {
  yield takeLatest(types.CATALOGUE_START, catalogueStart);
}
