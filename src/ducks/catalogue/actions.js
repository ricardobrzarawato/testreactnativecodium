import * as types from "./types";

const catalogueStart = (url, callback) => ({
  type: types.CATALOGUE_START,
  payload: {
    url,
    callback
  }
});

export default catalogueStart;
