import * as types from "./types";

const initialState = {};

export default function catalogueStore(state = initialState, action) {
  switch (action.type) {
    case types.CATALOGUE_START:
      return {
        loading: true,
        data: [],
        url: action.payload.url
      };
    case types.CATALOGUE_COMPLETE:
      return {
        loading: false,
        data: action.result,
        url: null
      };
    case types.CATALOGUE_ERROR:
      return {
        loading: false,
        data: [],
        url: null
      };
    default:
      return state;
  }
}
